//
//  ViewController.swift
//  TextReader
//
//  Created by Nikita Kryuchkov on 30.09.2021.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let contents = readFile(name: "forRead")
        print(contents)
    }
    
    func readFile(name: String) -> [[MyStruct]] {
        var result: [[MyStruct]] = []
        
        if let fileURlProject = Bundle.main.path(forResource: name, ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: fileURlProject, encoding: String.Encoding.utf8)
                result = try convertToArray(input: contents)
            } catch  {
                print(error.localizedDescription)
            }
        }
        return result
    }
    
    func convertToArray(input: String) throws -> [[MyStruct]] {
        let rows = input.components(separatedBy: .newlines)
        guard !rows.isEmpty else { throw InvalidValue(reason: "source") }
        var result: [[MyStruct]] = []
        var index = 0
        
        while index < rows.count - 1 {
            guard let count = Int(rows[index]) else { throw InvalidValue(reason: "count") }
            
            let data: [MyStruct]  = try rows[(index + 1)..<(index + count + 1)]
                .map {
                    return try MyStruct(from: $0)
                }
            result.append(data)
            index += count + 1
        }
        return result
    }
}

struct InvalidValue: Error { var reason: String }

struct MyStruct {
    var name: String
    var x, y, w, h: Float
    
    init(from string: String) throws {
        let values = string.components(separatedBy: ";")
        guard values.count == 5 else { throw InvalidValue(reason: "count in row") }
        name = values[0]
        guard
            let x = Float(values[1]), let y = Float(values[2]),
            let w = Float(values[3]), let h = Float(values[4])
        else { throw InvalidValue(reason: "float") }
        self.x = x; self.y = y; self.w = w; self.h = h
    }
}
